import React, {Fragment} from 'react';

import AppNavbar from './components/AppNavbar'; 
// import CourseCard from './components/CourseCard';
// import Welcome from './components/Welcome';
import Home from './pages/Home';
import Courses from './pages/Courses'

export default function App(){

	return (

		<Fragment>
			<AppNavbar/>
			<Home/>
			{/*<CourseCard/>*/}
			<Courses/>
		</Fragment>

	)
}