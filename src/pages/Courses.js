
import React from 'react';

/*react-bootstrap component*/
import {Container} from 'react-bootstrap';

/*components*/
import Course from './../components/Course';

/*mock data*/
import courses from './../mock-data/courses';

export default function Courses(){
	let CourseCards = courses.map((course)=>{
		// console.log(course)
		return <Course course = {course}/>

		//passing (course placeholder) an argument to Course component

		//courses.map -> mapping the array of objects in courses.js
	})

	return (
		<Container fluid>
			{CourseCards}
		</Container>
	)
}