import React, {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types'; //validation or sends and error in the console if a missing data or value is not entered or present in the mock data. (app might still work despite the error - will only show the error message in the console)

export default function Course(props){
	console.log(props);
	// console.log(props.cor1.name)
	
	let course = props.course;

	//state
	const [count, setCount] = useState(10);

	function enroll(){
		if(count == 0){
			alert("No seats available");
		} else {
			setCount(count - 1);
		}
	}


	return (
		<Card>
		  <Card.Body>
		    <Card.Title>{course.name}</Card.Title>
		    <Card.Text>
		    <p> Description: </p>
		    <p>{course.description}</p>
		    <p>Price:</p>
		    <p>{course.price}</p>
		    <h5>{count} Enrollees</h5>
		    <p></p>
		    </Card.Text>
		    <Button variant="primary" onClick = {enroll}>Enroll</Button>
		  </Card.Body>
		</Card>	

	)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}