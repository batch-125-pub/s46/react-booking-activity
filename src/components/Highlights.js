import React from 'react';


import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function Highlights(){
	return (

		<Row className = "g-0">
			<Col className= "p-0" xs={12} md={4}>
				<Card className = "mb-3">
				  <Card.Body>
				    <Card.Title>Learn from Home</Card.Title>
				    <Card.Text>
				      	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      	tempor incididunt ut labore et dolore magna aliqua. 
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
				
			<Col className= "p-0" xs={12} md={4}>
				<Card className = "mb-3">
				  <Card.Body>
				    <Card.Title>Study Now, Pay Later</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      	tempor incididunt ut labore et dolore magna aliqua. 
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
				
			<Col className= "p-0" xs={12} md={4}>
				<Card  className = "mb-3">
				  <Card.Body>
				    <Card.Title>Be Part of a Community</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      	tempor incididunt ut labore et dolore magna aliqua. 
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>

/*
		<Card>
		  <Card.Body>
		    <Card.Title>Card Title</Card.Title>
		    <Card.Text>
		      Some quick example text to build on the card title and make up the bulk of
		      the card's content.
		    </Card.Text>
		    <Button variant="primary">Go somewhere</Button>
		  </Card.Body>
		</Card>
*/

	)

}