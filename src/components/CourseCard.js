import React from 'react';


import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap';

export default function CourseCard(){
	return (
		<Row className = "g-0">
			<Col className= "p-0">
				<Card>
				  <Card.Body>
				    <Card.Title>Sample Course</Card.Title>
				    <Card.Text>
				    <p>Description:</p>
				      	<p>	This is a sample course offering.</p>
				      <p>Price</p>
				      <p>Php 40,000</p>
				    </Card.Text>
				    <Button variant="primary">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}
