import React from 'react';

/*importing react bootstrap*/

// other way of importing
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
//or type in the following (without specifically locating inside Navbar or Nav):

import {Nav, Navbar} from 'react-bootstrap';

/*app navbar*/

export default function AppNavbar(){
  return (
    
    <Navbar variant="dark" bg="dark" expand="lg">
      <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#link">Link</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar> 

     
    )
}